# Concrete Solutions Challenge Kata 09 && WhatsApp Automation

  To solution the code kata 09 exercise I used TDD technique, first I created the test and after developed the feature.
  I used RSpec to create tests.

  Automation of WhatsApp Web, using the BDD framework Cucumber to provide documentation to automated tests.


## Dependencies

* Ruby 2.2.2

### Recommendations

* Use Rbenv to handle rubies and gemsets

  * See installation instructions here (https://github.com/rbenv/rbenv)
  * And here if you have Linux enviroment (https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-14-04https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-14-04)

  * Install bundler

              $ gem install bundler

  * Project Installation

              $ bundle install

  * How to run cucumber features

              $ bundle exec cucumber features/ (for all features and scenarios)

                                          or

              $ bundle exec cucumber features/<name_feature> (for specific feature)


### Project Installation / Running tests Kata 09

            * Inside project folder run run_specs.sh file
            $ sh run_specs.sh
