# encoding: utf-8
# language: pt

Funcionalidade: Criação de grupos no WhatsApp Web

Cenário: Criar grupo com um único contato
  Dado que esteja na página do whatsapp web
  Quando selecionar e preencher os dados para criação de um grupo
  Então o grupo será criado

Cenário: Criar grupo com todos contatos disponíveis
  Dado que esteja na página do whatsapp web
  Quando selecionar para criar um grupo
  E selecionar todos contatos da agenda
  Então o grupo será criado
