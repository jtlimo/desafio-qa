# encoding: utf-8
# language: pt

Funcionalidade: Exclusão de grupos no whatsapp web

Contexto:
  Dado que existam grupos criados

Cenário: Excluir grupo
  Dado que esteja na página do whatsapp web
  Quando selecionar um grupo para excluí-lo
  Então o grupo será excluído
