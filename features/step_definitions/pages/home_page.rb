# HomePage class
class HomePage < SitePrism::Page
  set_url "http://#{USERNAME}:#{PASSWORD}@#{WHATSAPP_URL}"

  element :menu_acoes, '.menu-item:last-child[0]' # probably dont work
  element :item_novo_grupo, "a[title='Novo grupo']"
  element :input_assunto_grupo, '.input-emoji[0]' # probably dont work
  element :button_confirm, 'btn.btn-round'
  element :find_contacts, 'input.input-line'
  element :contact, "span[title='Aline Not Sóbria']"

  # Fazendo o basico, pois nao tem como realmente testar
  # o whatsapp web
  def create_group
    menu_acoes.click
    item_novo_grupo.click
    input_assunto_grupo.set 'Test'
    button_confirm.click
    find_contacts.set 'Aline not'
    contact.click
  end
end
