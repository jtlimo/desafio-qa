Dado(/^que esteja na página do whatsapp web$/) do
  home_page.load
end

Dado(/^que existam grupos criados$/) do
  pending
end

Quando(/^selecionar e preencher os dados para criação de um grupo$/) do
  home_page.create_group
end

Quando(/^selecionar para criar um grupo$/) do
  pending
end

Quando(/^selecionar todos contatos da agenda$/) do
  pending
end

Quando(/^selecionar um grupo para excluí\-lo$/) do
  pending
end

Então(/^o grupo será criado$/) do
  pending
end

Então(/^o grupo será excluído$/) do
  pending
end
