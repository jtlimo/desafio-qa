require 'rspec/expectations'
require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara/window'
require 'selenium-webdriver'
require 'site_prism'
require 'pry'
require 'data_magic'
World(RSpec::Matchers)

env = %w(qa).include?(ENV['ENV']) ? ENV['ENV'].to_sym : :qa

Dir['../.././pages/*.rb'].each { |file| require file }

SitePrism.configure do |config|
  config.use_implicit_waits = true
end

AMBIENTES_CONFIG = {
  qa: ['web.whatsapp.com']
}.freeze

CONFIG = {
  qa:  %w(user password)
}.freeze

WHATSAPP_URL = AMBIENTES_CONFIG.fetch(env)[0]
USERNAME = CONFIG.fetch(env)[0]
PASSWORD = CONFIG.fetch(env)[1]
