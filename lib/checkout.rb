# Sum values and return total
class Checkout
  attr_reader :unit_prices, :special_prices, :total

  def initialize
    @total = []
    @unit_prices = { 'A' => 50, 'B' => 30, 'C' => 20, 'D' => 15 }
    @special_prices = { 'A' => (@unit_prices['A'] - @unit_prices['B']),
                        'B' => (@unit_prices['B'] - @unit_prices['D']) }
  end

  def scan(values)
    val = values.split(//)
    val.each do |item|
      @total << @unit_prices[item]
    end
    @total = (@total.inject { |acc, elem| acc + elem } if counting?(val))
    @discount = @discount.inject { |acc, elem| acc + elem } unless @discount.nil?
    @total -= @discount unless @discount.nil?
    @total = 0 if @total.nil?
  end

  def count_strings(array)
    array.each_with_object(Hash.new(0)) do |string, hash|
      hash[string] += 1
    end
  end

  def counting?(array)
    @discount = []
    h_b = { 4 => 2, 8 => 3, 16 => 4 }
    h_a = { 6 => 2, 9 => 3, 12 => 4 }
    count = count_strings(array)
    @special_prices.any? do |_k|
      count.each_pair do |key, it|
        @discount << @special_prices[key] if key == 'A' && it > 2 &&
                                             h_a[it].nil?
        @discount << h_a[it] * @special_prices[key] if key == 'A' &&
                                                       !h_a[it].nil?
        @discount << @special_prices[key] if key == 'B' && it > 1 &&
                                             h_b[it].nil?
        @discount << h_b[it] * @special_prices[key] if key == 'B' &&
                                                       !h_b[it].nil?
      end
    end
  end
end
