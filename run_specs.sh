#!/bin/bash
echo "Installing required gems..."
gem install bundler
bundle install

echo "Running RSpec tests...:"

bin/rspec  --format doc
