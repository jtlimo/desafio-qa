require 'checkout'

RSpec.describe Checkout, '#total' do
  context 'validate how much cost invalid values' do
    it 'scans itens and doesnt sum the values' do
      numbers = Checkout.new
      numbers.scan('')
      expect(numbers.total).to eq 0
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost single scans' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('A')
      expect(numbers.total).to eq 50
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AB scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AB')
      expect(numbers.total).to eq 80
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost CDBA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('CDBA')
      expect(numbers.total).to eq 115
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AA')
      expect(numbers.total).to eq 100
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAA')
      expect(numbers.total).to eq 130
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAAA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAAA')
      expect(numbers.total).to eq 180
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAAAA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAAAA')
      expect(numbers.total).to eq 230
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAAAAA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAAAAA')
      expect(numbers.total).to eq 260
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAAB scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAAB')
      expect(numbers.total).to eq 160
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAABB scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAABB')
      expect(numbers.total).to eq 175
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost AAABBD scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('AAABBD')
      expect(numbers.total).to eq 190
    end
  end
end

RSpec.describe Checkout, '#total' do
  context 'validate how much cost DABABA scan' do
    it 'scans itens and sum the values' do
      numbers = Checkout.new
      numbers.scan('DABABA')
      expect(numbers.total).to eq 190
    end
  end
end
